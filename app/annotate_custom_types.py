from typing import TypeVar
from aiopg.sa import Engine
from app.database.essences import UserObject
from aiohttp. web_routedef import RouteDef
from aiohttp.web import Application, Response, Request

db_engine = TypeVar('db_engine', bound = Engine)
user_type = TypeVar('user_type', bound = UserObject)
route_def_type = TypeVar('route_def_type', bound = RouteDef)
app_type = TypeVar('app_type', bound = Application)
request_type = TypeVar('request_type', bound = Request)
response_type = TypeVar('response_type', bound = Response)
