from typing import Optional, List
from app.database import UserByDB
from app.annotate_custom_types import db_engine, user_type


class UserLogic(object):

    async def get_users(self, ids: Optional[List[int]], engine: db_engine) -> List[user_type]:
        user_db = UserByDB(engine = engine)
        if not ids:
            return await user_db.get_all_users()
        return await user_db.get_filtered_users(ids)
