import pytz
import sqlalchemy as sa
from typing import TypeVar
from dataclasses import dataclass
from datetime import datetime

metadata = sa.MetaData(schema="bucket")

users = sa.Table("users", metadata,
                 sa.Column("uuid", sa.Integer, primary_key = True),
                 sa.Column("firstname", sa.String(50), nullable = False),
                 sa.Column("surname", sa.String(50), nullable = False),
                 sa.Column("middlename", sa.String(50)),
                 sa.Column("fio", sa.Text, nullable = False),
                 sa.Column("sex", sa.String(20), nullable = False),
                 sa.Column("age", sa.Integer, nullable = False),
                 sa.Column("followers", sa.ARRAY(sa.Integer)),
                 sa.Column("created_date", sa.DateTime(timezone = pytz.UTC)),
                 sa.Column("updated_date", sa.DateTime(timezone = pytz.UTC)))


@dataclass
class UserObject:
    uuid: int
    firstname: str
    surname: str
    middlename: str
    fio: str
    sex: str
    age: int
    followers: list
    created_date: str
    updated_date: str


orders = sa.Table("orders", metadata,
                  sa.Column("uuid", sa.Integer, primary_key = True),
                  sa.Column("number", sa.Text, nullable = False),
                  sa.Column("additional_information", sa.Text),
                  sa.Column("user_id", sa.ForeignKey("users.uuid"), nullable = False),
                  sa.Column("created_date", sa.DateTime(timezone = pytz.UTC)),
                  sa.Column("updated_date", sa.DateTime(timezone = pytz.UTC)),
                  sa.Column("is_closed", sa.Boolean))


@dataclass
class OrderObject:
    id: int
    number: str
    additions_information: str
    user_id: int
    created_date: datetime
    updated_date: datetime
    id_closed: bool


order_obj = TypeVar('order_obj', bound = OrderObject)

products = sa.Table("products", metadata,
                    sa.Column("uuid", sa.Integer, primary_key = True),
                    sa.Column("name", sa.String(100), nullable = False),
                    sa.Column("description", sa.Text),
                    sa.Column("price", sa.Integer, nullable = False),
                    sa.Column("left_in_stock", sa.Integer, nullable = False),
                    sa.Column("created_date", sa.DateTime(timezone = pytz.UTC)),
                    sa.Column("updated_date", sa.DateTime(timezone = pytz.UTC)))


@dataclass
class ProductObject:
    id: int
    name: str
    description: str
    price: int
    left_in_stock: int
    created_date: datetime
    updated_date: datetime


product_obj = TypeVar('product_obj', bound = ProductObject)

orders_products = sa.Table("orders_products", metadata,
                           sa.Column("uuid", sa.Integer, primary_key = True),
                           sa.Column("order_id", sa.ForeignKey("orders.uuid"), nullable = False),
                           sa.Column("product_id", sa.ForeignKey("products.uuid"), nullable = False))
