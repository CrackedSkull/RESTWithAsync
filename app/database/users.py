from typing import Union, List, NoReturn

from .essences import users, UserObject


class UserByDB(object):
    def __init__(self, engine) -> NoReturn:
        self._engine = engine

    async def get_conn(self):
        async with self._engine.acquire() as conn:
            return conn

    async def get_all_users(self):
        users_array = []
        conn = await self.get_conn()
        async for row in conn.execute(users.select()):
            users_array.append(UserObject(uuid = row.uuid,
                                          firstname = row.firstname,
                                          surname = row.surname,
                                          middlename = row.middlename,
                                          fio = row.fio,
                                          sex = row.sex,
                                          age = row.age,
                                          followers = row.followers,
                                          created_date = str(row.created_date),
                                          updated_date = str(row.updated_date)))
        return users_array

    async def get_filtered_users(self, ids: List[int]):
        users_array = []
        conn = await self.get_conn()
        async for row in conn.execute(users.select(users.c.uuid.in_(tuple(ids)))):
            users_array.append(UserObject(uuid = row.uuid,
                                          firstname = row.firstname,
                                          surname = row.surname,
                                          middlename = row.middlename,
                                          fio = row.fio,
                                          sex = row.sex,
                                          age = row.age,
                                          followers = row.followers,
                                          created_date = str(row.created_date),
                                          updated_date = str(row.updated_date)))
        return users_array
