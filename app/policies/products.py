from .handler import AbstractBaseHandler


class ProductHandler(AbstractBaseHandler):

	async def get_handler(self, request):
		return "call read product logic"

	async def post_handler(self, request):
		return "call create product logic"

	async def patch_handler(self, request):
		return "call update product logic"

	async def delete_handler(self, request):
		return "call delete product logic"
