import json
from aiohttp import web
from abc import ABC
from app.annotate_custom_types import request_type, response_type


class AbstractBaseHandler(ABC):

	async def handler(self, request: request_type) -> response_type:
		if request.method == "GET":
			response_obj = await self.get_handler(request)
		elif request.method == "POST":
			response_obj = await self.post_handler(request)
		elif request.method == "PATCH":
			response_obj = await self.patch_handler(request)
		elif request.method == "DELETE":
			response_obj = await self.patch_handler(request)
		else:
			response_obj = web.Response(text=json.dumps("Invalid method"), status = 405)
		return response_obj

	async def get_handler(self, request: request_type) -> response_type:
		raise NotImplementedError

	async def post_handler(self, request: request_type) -> response_type:
		raise NotImplementedError

	async def patch_handler(self, request: request_type) -> response_type:
		raise NotImplementedError

	async def delete_handler(self, request: request_type) -> response_type:
		raise NotImplementedError
