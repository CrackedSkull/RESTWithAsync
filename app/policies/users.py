import json
from aiohttp import web
from .handler import AbstractBaseHandler
from app.services import UserLogic
from dataclasses import asdict
from app.annotate_custom_types import request_type, response_type


class UserHandler(AbstractBaseHandler):

	async def get_handler(self, request: request_type) -> response_type:
		ids = request.query.getone("ids", None)
		if ids:
			try:
				ids = list(map(int, ids.split(",")))
			except ValueError:
				return web.Response(text = json.dumps("Invalid parameters"), status = 415)
		answer = await UserLogic().get_users(engine = request.app["pg_engine"], ids = ids)
		if answer:
			answer = [asdict(obj) for obj in answer]
			return web.Response(body = json.dumps(answer), status = 200)
		return web.Response(text = json.dumps("No users were found"), status = 404)

	async def post_handler(self, request):
		return "call create user logic"

	async def patch_handler(self, request):
		return "call update user logic"

	async def delete_handler(self, request):
		return "call delete user logic"
