from .handler import AbstractBaseHandler


class OrderHandler(AbstractBaseHandler):

	async def get_handler(self, request):
		return "call read order logic"

	async def post_handler(self, request):
		return "call create order logic"

	async def patch_handler(self, request):
		return "call update order logic"

	async def delete_handler(self, request):
		return "call delete order logic"
