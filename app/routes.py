from typing import List, NoReturn
from aiohttp. web_routedef import RouteDef
from app.policies import UserHandler, OrderHandler, ProductHandler
from dataclasses import dataclass


@dataclass
class Route:
    methods: list
    handler: object
    path: str
    kwargs: dict


__data_for_pattern_constructor = [
    Route(methods = ["get", "post", "patch", "delete"],
          handler = UserHandler().handler, path = "/user", kwargs = {}),
    Route(methods = ["get", "post", "patch", "delete"],
          handler = OrderHandler().handler, path = "/order", kwargs = {}),
    Route(methods = ["get", "post", "patch", "delete"],
          handler = ProductHandler().handler, path = "/product", kwargs = {})
]


class PatternCreator(object):
    def __init__(self, data: List[Route]) -> NoReturn:
        self._data = data

    @property
    def create_patterns(self) -> List[RouteDef]:
        _patterns = []
        for data in self._data:
            for method in data.methods:
                _patterns.append(
                    RouteDef(method=method, handler=data.handler,
                             path="/api" + data.path, kwargs = data.kwargs)
                )
        return _patterns


patterns = PatternCreator(__data_for_pattern_constructor).create_patterns
