\connect shop;

INSERT INTO bucket.users (uuid, firstname, surname, middlename, fio, sex, age) VALUES
(1, 'Oleg', 'Petrovich', 'Ivanovich', 'Petrovich Oleg Ivanovich', 'male', 56),
(2, 'Olga', 'Barabasova', 'Anatolievna', 'Barabasova Olga Anatolievna', 'female', 23),
(3, 'Steve', 'Smith', NULL, 'Smith Steve', 'male', 18),
(4, 'It', 'It', 'It', 'It It It', 'it', 1024);
