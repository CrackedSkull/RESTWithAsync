CREATE DATABASE shop;

\connect shop;

CREATE SCHEMA bucket;
CREATE TABLE bucket.users (
    uuid SERIAL PRIMARY KEY,
    firstname CHARACTER VARYING (50) NOT NULL,
    surname CHARACTER VARYING (50) NOT NULL,
    middlename CHARACTER VARYING (50),
    fio TEXT NOT NULL,
    sex CHARACTER VARYING (20) NOT NULL,
    age INTEGER NOT NULL,
    followers INTEGER ARRAY,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

COMMENT ON TABLE bucket.users IS
'Information about users.';

CREATE TABLE bucket.orders (
    uuid SERIAL PRIMARY KEY,
    number TEXT NOT NULL,
    additional_information TEXT,
    user_id INTEGER NOT NULL REFERENCES bucket.users(uuid) ON DELETE CASCADE,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    is_closed BOOLEAN DEFAULT FALSE
);

COMMENT ON TABLE bucket.orders IS
'Information about users orders.';

CREATE TABLE bucket.products (
    uuid SERIAL PRIMARY KEY,
    name CHARACTER VARYING (100)NOT NULL,
    description TEXT,
    price INTEGER NOT NULL,
    left_in_stock INTEGER NOT NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

COMMENT ON TABLE bucket.products IS
'Information about products.';

CREATE TABLE bucket.orders_products (
    uuid SERIAL PRIMARY KEY,
    order_id INTEGER NOT NULL REFERENCES bucket.orders(uuid) ON DELETE CASCADE,
    product_id INTEGER NOT NULL REFERENCES bucket.products(uuid) ON DELETE  CASCADE
);

COMMENT ON TABLE bucket.orders_products IS
'Information about orders and products relation';