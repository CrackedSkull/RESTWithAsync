from typing import NoReturn
from aiohttp import web
from aiopg.sa import create_engine
from aiohttp_swagger import *
from app.routes import patterns
from app.annotate_custom_types import app_type


async def pg_engine(application: app_type) -> NoReturn:
	application["pg_engine"] = await create_engine(
		user="postgres_admin",
		password="CustomerAndProduct13",
		database = "shop",
		host="localhost",
		port=5432
	)
	yield
	application["pg_engine"].close()
	await application["pg_engine"].wait_closed()

app = web.Application()
app.cleanup_ctx.append(pg_engine)
app.router.add_routes(routes = patterns)

setup_swagger(app = app, swagger_from_file = "app/swagger_setup.yaml", swagger_url = "/api")

web.run_app(app = app, host = "localhost", port=8000)
